import {useState,useEffect} from 'react'
import Frame from './components/frame'
import Ball from './components/ball'
import './App.css';
function App() {
  const colors = ['red','purple','orange','green','blue','white','skyblue','pink','indigo','yellow']
  const [posX,setPosX] = useState(0)
  const [posY,setPosY] = useState(0)
  const [wayX,setWayX] = useState(false)
  const [wayY,setWayY] = useState(false)
  const [color,setColor] = useState(0)
  const [colorWay,setColorWay] = useState(false)
  const handleMoveX = () =>{
   
    let step = Math.floor(Math.random() * 100)
    let x = posX;
    if(!colorWay){
      let index = color + 1     
      setColor(index)
      if(color === 9){
        setColorWay(true)
        setColor(8)
      }
    } else{
      let index = color - 1      
      setColor(index)
      if(color === 0){
        setColorWay(false)
        setColor(1)
      }
    }
    if(x < 581 && !wayX){
      if(posX + step > 580){
        setPosX(580)
        setWayX(true)
      }else{
        x+=step
        setPosX(x)
      }
    } else{
        if(posX - step < 0){
          setPosX(0)
          setWayX(false)          
        }else{
          x-=step
          setPosX(x)
      }
    }
  }
  const handleMoveY = () =>{    
    let step = Math.floor(Math.random() * 100)
    let y = posY;
    if(y < 231 && !wayY){
      if(posY + step > 231){
        setPosY(230)
        setWayY(true)        
      }else{
        y+=step
        setPosY(y)
      }
    } else{
        if(posY - step < 0){
          setPosY(0)
          setWayY(false)    
        }else{
          y-=step
          setPosY(y)
      }
    }
  }

  const handleResetColor = () =>{
    setColor(0)
    setColorWay(false)
  }

  return (
    <div className="App">
        <Frame>
          <Ball color={colors[color]} pos={{posX,posY}}/>
        </Frame>
        <div className="buttons">
          <button className="btn move" onClick={()=>{
            handleMoveX()
            handleMoveY()
          }}>Move</button>
          <button onClick={()=>{handleResetColor()}} className="btn reset">Reset</button>
        </div>
    </div>
  );
}

export default App;
