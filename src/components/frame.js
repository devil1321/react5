import React from 'react'

const Frame = (props) => {
    return (
        <div className="frame">
            {props.children}
        </div>
    )
}
export default Frame