import React from 'react'

const Ball = (props) => {
    return (
        <div style={{
            left:props.pos.posX,
            top:props.pos.posY,
            backgroundColor:props.color
            }} 
        className="ball"></div>
    )
}
export default Ball